#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2
#define TILE_WIDTH 512
#define TILE_HEIGHT 512

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    for(int i = 0; i < filters; i++)
    {
    int *copy = new int[height * width];

    for(int h_tile=0; h_tile <height ; h_tile += TILE_HEIGHT)
	{
	    for(int w_tile=0; w_tile < width ; w_tile += TILE_WIDTH)
	    {
		for(int h = max(h_tile-1, 0); h < min(height, h_tile + TILE_HEIGHT +1); ++h)
		{
		    for(int w = max(w_tile-1, 0); w < min(width, w_tile + TILE_WIDTH +1); ++w)
		    {
			// Calculating copy[h * width + w]
            if(w>0 && w<width-1 && h>0 && h < width -1)
            {
                copy[h * width + w] = 0;
                copy[h * width + w] += image[(-1 + h) * width + -1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + -1) * FILTER_SIZE + 1 + -1];
                copy[h * width + w] += image[(-1 + h) * width + 0 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + -1) * FILTER_SIZE + 1 + 0];
                copy[h * width + w] += image[(-1 + h) * width + 1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + -1) * FILTER_SIZE + 1 + 1];
                copy[h * width + w] += image[(0 + h) * width + -1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 0) * FILTER_SIZE + 1 + -1];
                copy[h * width + w] += image[(0 + h) * width + 0 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 0) * FILTER_SIZE + 1 + 0];
                copy[h * width + w] += image[(0 + h) * width + 1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 0) * FILTER_SIZE + 1 + 1];
                copy[h * width + w] += image[(1 + h) * width + -1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 1) * FILTER_SIZE + 1 + -1];
                copy[h * width + w] += image[(1 + h) * width + 0 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 1) * FILTER_SIZE + 1 + 0];
                copy[h * width + w] += image[(1 + h) * width + 1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 1) * FILTER_SIZE + 1 + 1];
            }
            else{
                copy[h * width + w] = 0;
                if(!(-1 + h < 0 || -1 + h >= height || -1 + w < 0 || -1 + w >= width))
                copy[h * width + w] += image[(-1 + h) * width + -1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + -1) * FILTER_SIZE + 1 + -1];
                if(!(-1 + h < 0 || -1 + h >= height || 0 + w < 0 || 0 + w >= width))
                copy[h * width + w] += image[(-1 + h) * width + 0 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + -1) * FILTER_SIZE + 1 + 0];
                if(!(-1 + h < 0 || -1 + h >= height || 1 + w < 0 || 1 + w >= width))
                copy[h * width + w] += image[(-1 + h) * width + 1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + -1) * FILTER_SIZE + 1 + 1];
                if(!(0 + h < 0 || 0 + h >= height || -1 + w < 0 || -1 + w >= width))
                copy[h * width + w] += image[(0 + h) * width + -1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 0) * FILTER_SIZE + 1 + -1];
                            if(!(0 + h < 0 || 0 + h >= height || 0 + w < 0 || 0 + w >= width))
                copy[h * width + w] += image[(0 + h) * width + 0 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 0) * FILTER_SIZE + 1 + 0];
                if(!(0 + h < 0 || 0 + h >= height || 1 + w < 0 || 1 + w >= width))
                copy[h * width + w] += image[(0 + h) * width + 1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 0) * FILTER_SIZE + 1 + 1];
                if(!(1 + h < 0 || 1 + h >= height || -1 + w < 0 || -1 + w >= width))
                copy[h * width + w] += image[(1 + h) * width + -1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 1) * FILTER_SIZE + 1 + -1];
                if(!(1 + h < 0 || 1 + h >= height || 0 + w < 0 || 0 + w >= width))
                copy[h * width + w] += image[(1 + h) * width + 0 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 1) * FILTER_SIZE + 1 + 0];
                if(!(1 + h < 0 || 1 + h >= height || 1 + w < 0 || 1 + w >= width))
                copy[h * width + w] += image[(1 + h) * width + 1 + w] *
                                        filter[i * FILTER_SIZE * FILTER_SIZE + (1 + 1) * FILTER_SIZE + 1 + 1];
            }
		    }
		}
	    }
	}
	 
	for(int h = 0; h < height; ++h)
        {
            for(int w = 0; w < width; ++w)
                image[h * width + w] = copy[h * width + w];
        }
        delete copy;
    }
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
